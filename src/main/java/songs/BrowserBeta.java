package songs;

import javax.swing.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BrowserBeta {



    static private String order;
    private List<Song> listOfSongs;

    public List<Song> getListOfSongs() {
        return listOfSongs;
    }

    public void setListOfSongs(List<Song> listOfSongs) {
        this.listOfSongs = listOfSongs;
    }

    BrowserBeta (List<Song> listOfSongs){
        this.listOfSongs = listOfSongs;

        }

    public static String getOrder() {
        return order;
    }

    public static void setOrder(String order) {
        BrowserBeta.order = order;
    }

    public static void listingArtistTitleLirycs(List<Song> listOfSongs) {
        for (Song songsObject : listOfSongs) {
            String songDescription = new StringBuilder()
                    .append(songsObject.getArtist())
                    .append(" '")
                    .append(songsObject.getTitle())
                    .append("'      \n\n>")
                    .append(songsObject.getLyrics())
                    .append("<    - size: ")
                    .append(songsObject.getLyrics().length())
                    .append(".\n-------------------------------------------------------\n")
                    .toString();
            System.out.println(songDescription);

        }
    }

    public List<Song> artistInAlphabeticalOrder() {

        order = "ARTISTS";
        listOfSongs.sort(Comparator.comparing(Song::getArtist));
        return listOfSongs;
    }

    public List<Song> titlesInAlphabeticalOrder() {

        order = "TITLES";
        listOfSongs.sort(Comparator.comparing(Song::getTitle));
        return listOfSongs;
    }


    public void listingSongsInOrder() {

        System.out.println("\nLISTING " + order + " IN ALPHABETICAL ORDER:");
        for (Song listInOrder : listOfSongs) {
            if (order.equals("ARTISTS")) {
                System.out.println(new StringBuilder()
                        .append("Artist: ")
                        .append(listInOrder.getArtist())
                        .append("  -  Title: '")
                        .append(listInOrder.getTitle())
                        .append("'")
                        .toString());
            } else if (order.equals("TITLES")) {
                System.out.println(new StringBuilder()
                        .append("Title: '")
                        .append(listInOrder.getTitle())
                        .append("'  -  Artist: ")
                        .append(listInOrder.getArtist())
                        .toString());
            }
        }

    }

    public void listingArtistsTitlesInTable() {

        String spaces = "                               ";
        listOfSongs.sort(Comparator.comparing(Song::getArtist));
        System.out.println("\nARTIST:" + spaces.substring(10) + "TITLE:");

        for (Song listInOrder : listOfSongs) {
            int spaceLength = listInOrder.getArtist().length();
            System.out.println(listInOrder.getArtist() + spaces.substring(spaceLength, 28) + listInOrder.getTitle());
        }
    }

    public List<Song> findByPhrase() {

        String phrase;
        do {
            phrase = JOptionPane.showInputDialog(null, "Enter the phrase:");
        } while (phrase.isEmpty());

        String searchingPhrase = phrase;
        List<Song> foundByPhrase = listOfSongs.stream()
                .filter(x -> x.getLyrics().toUpperCase().contains(searchingPhrase.toUpperCase()))
                .collect(Collectors.toList());

        System.out.println(new StringBuilder()
                .append("\nFOUND ")
                .append(foundByPhrase.size())
                .append(" SONG(S) WITH PHRASE ")
                .append(">")
                .append(searchingPhrase)
                .append("< :")
                .toString());
        return foundByPhrase;
    }
}

