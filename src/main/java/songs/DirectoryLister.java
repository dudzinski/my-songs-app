package songs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryLister {

    //  file's name format:  artist - title.txt

    public List<String> fileReader(String path) {

        List<String> listOfFiles = new ArrayList<>();
        File file = new File(path);

            File[] files = file.listFiles();
            if (files == null){
                System.out.println("Load error!");
                System.exit(0);}
                for (File f : files) {
            listOfFiles.add(f.getAbsolutePath());
        }
        return listOfFiles;
    }
}
