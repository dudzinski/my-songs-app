package songs;

public class Song {

    private String artist;
    private String title;
    private String lyricsString;

    public Song(String artist, String title, String lyricsString) {
        this.artist = artist;
        this.title = title;
        this.lyricsString = lyricsString;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLyrics() {
        return lyricsString;
    }

    public void setLyrics(String lyricsString) {
        this.lyricsString = this.lyricsString;
    }
}
