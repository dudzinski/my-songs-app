package songs;

import javax.swing.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Browser {

    static private String order;

    public static String getOrder() {
        return order;
    }

    public static void setOrder(String order) {
        Browser.order = order;
    }

    public static void listingArtistTitleLirycs(List<Song> listOfSongs) {
        for (Song songsObject : listOfSongs) {
            String songDescription = new StringBuilder()
                    .append(songsObject.getArtist())
                    .append(" '")
                    .append(songsObject.getTitle())
                    .append("'      \n\n>")
                    .append(songsObject.getLyrics())
                    .append("<    - size: ")
                    .append(songsObject.getLyrics().length())
                    .append(".\n-------------------------------------------------------\n")
                    .toString();
            System.out.println(songDescription);

        }
    }

    public List<Song> artistInAlphabeticalOrder(List<Song> songsObject) {

        order = "ARTISTS";
        songsObject.sort(Comparator.comparing(Song::getArtist));
        return songsObject;
    }

    public List<Song> titlesInAlphabeticalOrder(List<Song> songsObject) {

        order = "TITLES";
        songsObject.sort(Comparator.comparing(Song::getTitle));
        return songsObject;
    }


    public void listingSongsInOrder(List<Song> orderedSongsList) {

        System.out.println("\nLISTING " + order + " IN ALPHABETICAL ORDER:");
        for (Song listOfSongs : orderedSongsList) {
            if (order.equals("ARTISTS")) {
                System.out.println(new StringBuilder()
                        .append("Artist: ")
                        .append(listOfSongs.getArtist())
                        .append("  -  Title: '")
                        .append(listOfSongs.getTitle())
                        .append("'")
                        .toString());
            } else if (order.equals("TITLES")) {
                System.out.println(new StringBuilder()
                        .append("Title: '")
                        .append(listOfSongs.getTitle())
                        .append("'  -  Artist: ")
                        .append(listOfSongs.getArtist())
                        .toString());
            }
        }

    }

    public void listingArtistsTitlesInTable(List<Song> orderedSongList) {

        String spaces = "                               ";
        orderedSongList.sort(Comparator.comparing(Song::getArtist));
        System.out.println("\nARTIST:" + spaces.substring(10) + "TITLE:");

        System.out.printf("");

        for (Song listOfSongs : orderedSongList) {
            int spaceLength = listOfSongs.getArtist().length();
            System.out.println(listOfSongs.getArtist() + spaces.substring(spaceLength, 28) + listOfSongs.getTitle());
        }
    }

    public List<Song> findByPhrase(List<Song> orderedSongList) {

        String phrase;
        do {
            phrase = JOptionPane.showInputDialog(null, "Enter the phrase:");
            if (phrase == null){
                System.exit(0);
            }
        } while (phrase.isEmpty());

        String searchingPhrase = phrase;
        List<Song> foundByPhrase = orderedSongList.stream()
                .filter(x -> x.getLyrics().toUpperCase().contains(searchingPhrase.toUpperCase()))
                .collect(Collectors.toList());

        System.out.println(new StringBuilder()
                .append("\nFOUND ")
                .append(foundByPhrase.size())
                .append(" SONG(S) WITH PHRASE ")
                .append(">")
                .append(searchingPhrase)
                .append("< :")
                .toString());
        return foundByPhrase;
    }
}

