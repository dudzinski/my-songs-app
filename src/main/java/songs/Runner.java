package songs;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Runner {


    public static void main(String[] args) throws IOException {

        String path = "C:\\Users\\15IAPDXK4\\Desktop\\Moje piosenki";

        DirectoryLister directoryLister = new DirectoryLister();
        SongsReader songsReader = new SongsReader();
        Browser browser = new Browser();
        List<Song> listOfSongs;


        listOfSongs = songsReader.lyricsReader(directoryLister.fileReader(path));
        mappingSongs(listOfSongs);

        System.out.println("TEST LIST");
        browser.listingArtistTitleLirycs(listOfSongs);

        browser.listingSongsInOrder(browser.artistInAlphabeticalOrder(listOfSongs));
        browser.listingSongsInOrder(browser.titlesInAlphabeticalOrder(listOfSongs));
        browser.listingArtistsTitlesInTable(listOfSongs);
        browser.listingArtistTitleLirycs(browser.findByPhrase(listOfSongs));

//        BrowserBeta browserBeta = new BrowserBeta(listOfSongs);
//        browserBeta.listingArtistsTitlesInTable();
//

//        Adder adder = new Adder();
//        adder.addNewFile();

        System.out.println("\nOK");
    }

    private static void mappingSongs(List<Song> listOfSongs) {

        String mapKey;
        Map<String, String> mapOfSongs = new HashMap<>();
        for (Song song : listOfSongs) {
            mapKey = new StringBuilder()
                    .append(song.getArtist())
                    .append("#")
                    .append(song.getTitle())
                    .toString();
            mapOfSongs.put(mapKey, song.getLyrics());

        }
    }
}
