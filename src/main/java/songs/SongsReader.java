package songs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SongsReader {

    private List<String> lyrics = new ArrayList<>();
    private List<Song> listOfSongs = new ArrayList<>();
    private String pathArtist;
    private String pathTitle;
    private String pathName;
    private Path path;

    public List<Song> lyricsReader(List<String> file) {

        for (int i = 0; i < file.size(); i++) {
            pathName = file.get(i);
            path = Paths.get(file.get(i));

            pathArtist = pathName.substring(pathName.lastIndexOf("\\") + 1, pathName.indexOf("-")).trim();
            pathTitle = pathName.substring(pathName.indexOf("-") + 1, pathName.lastIndexOf(".")).trim();

            String lyricsString = "";
            try {
                lyrics = Files.readAllLines(path);
                lyricsString = String.join("\n", lyrics);
            } catch (IOException e) {
                System.out.println("Sprawdź swój plik!");
                System.exit(0);
            } catch (NumberFormatException n) {
                System.out.println("Niepoprawny format danych");
                System.exit(0);
            }
            listOfSongs.add(new Song(pathArtist, pathTitle, lyricsString));
        }
        return listOfSongs;
    }
}
